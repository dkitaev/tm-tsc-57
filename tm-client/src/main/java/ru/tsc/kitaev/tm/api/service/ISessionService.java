package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.endpoint.SessionDTO;

public interface ISessionService {

    @Nullable
    SessionDTO getSession();

    void setSession(@Nullable final SessionDTO session);

}
