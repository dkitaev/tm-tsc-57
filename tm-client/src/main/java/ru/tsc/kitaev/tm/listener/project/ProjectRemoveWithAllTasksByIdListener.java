package ru.tsc.kitaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveWithAllTasksByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-remove-with-tasks-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project with tasks by id...";
    }

    @Override
    @EventListener(condition = "@projectRemoveWithAllTasksByIdListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (projectEndpoint.findProjectById(sessionService.getSession(), projectId) == null) throw new ProjectNotFoundException();
        projectTaskEndpoint.removeById(sessionService.getSession(), projectId);
    }

}
