package ru.tsc.kitaev.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:config.properties")
public class PropertyService implements IPropertyService {

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['password.iteration']}")
    public Integer passwordIteration;

    @Value("#{environment['application.version']}")
    public String applicationVersion;

    @Value("#{environment['developer.name']}")
    public String developerName;

    @Value("#{environment['developer.email']}")
    public String developerEmail;

}
