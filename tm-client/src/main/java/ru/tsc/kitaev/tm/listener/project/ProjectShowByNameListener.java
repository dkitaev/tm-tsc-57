package ru.tsc.kitaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.endpoint.ProjectDTO;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class ProjectShowByNameListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-show-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by name...";
    }

    @Override
    @EventListener(condition = "@projectShowByNameListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = projectEndpoint.findProjectByName(sessionService.getSession(), name);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
