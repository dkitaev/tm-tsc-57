package ru.tsc.kitaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class ProjectStartByNameListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-start-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Start project by name...";
    }

    @Override
    @EventListener(condition = "@projectStartByNameListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        projectEndpoint.startProjectByName(sessionService.getSession(), name);
    }

}
