package ru.tsc.kitaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIndexListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-update-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by index...";
    }

    @Override
    @EventListener(condition = "@projectUpdateByIndexListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter index");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!projectEndpoint.existsProjectByIndex(sessionService.getSession(), index)) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @NotNull final String description = TerminalUtil.nextLine();
        projectEndpoint.updateProjectByIndex(sessionService.getSession(), index, name, description);
    }

}
