package ru.tsc.kitaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.endpoint.ProjectDTO;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIndexListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String command() {
        return "project-show-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by index...";
    }

    @Override
    @EventListener(condition = "@projectShowByIndexListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final ProjectDTO project = projectEndpoint.findProjectByIndex(sessionService.getSession(), index);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
