package ru.tsc.kitaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.endpoint.TaskDTO;
import ru.tsc.kitaev.tm.enumerated.Sort;
import ru.tsc.kitaev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListShowListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list...";
    }

    @Override
    @EventListener(condition = "@taskListShowListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @NotNull final List<TaskDTO> tasks;
        System.out.println("[SHOW PROJECTS]");
        if (sort.isEmpty()) tasks = taskEndpoint.findAllTask(sessionService.getSession());
        else {
            @NotNull Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskEndpoint.findAllTaskSorted(sessionService.getSession(), sort);
        }
        for (@NotNull final TaskDTO task: tasks) {
            System.out.println(tasks.indexOf(task) + 1 + ". " + task.toString() + ". " + task.getStatus());
        }
        System.out.println("[OK]");
    }

}
