package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.marker.UnitCategory;
import ru.tsc.kitaev.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class UserRepositoryTest {

    @NotNull
    @Autowired
    private EntityManager entityManager;

    @NotNull
    @Autowired
    private IUserDTORepository userRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    private final UserDTO user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "userTest@mail.com";

    public UserRepositoryTest() {
        user = new UserDTO();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        @NotNull final String password = "userTest";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
    }

    @Before
    public void before() {
        entityManager.getTransaction().begin();
        userRepository.add(user);
        entityManager.getTransaction().commit();
    }

    @Test
    @Category(UnitCategory.class)
    public void findByUserTest() {
        Assert.assertEquals(user, userRepository.findById(userId));
        Assert.assertEquals(user, userRepository.findByIndex(0));
        Assert.assertEquals(user, userRepository.findByLogin(userLogin));
        Assert.assertEquals(user, userRepository.findByEmail(userEmail));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTest() {
        entityManager.getTransaction().begin();
        userRepository.remove(user);
        entityManager.getTransaction().commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @After
    public void after() {
        entityManager.getTransaction().begin();
        userRepository.remove(user);
        entityManager.getTransaction().commit();
    }

}
