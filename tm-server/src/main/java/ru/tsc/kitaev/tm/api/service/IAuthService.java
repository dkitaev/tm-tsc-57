package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.dto.UserDTO;

import java.sql.SQLException;

public interface IAuthService {

    @Nullable
    UserDTO getUser() throws SQLException;

    @NotNull
    String getUserId();

    boolean isAuth();

    void logout();

    void login(@Nullable String login, @Nullable String password) throws SQLException;

    void registry(@Nullable String login, @Nullable String password, @Nullable String email) throws SQLException;

    void checkRoles(Role @Nullable...roles) throws SQLException;

}
