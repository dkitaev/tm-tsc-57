package ru.tsc.kitaev.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:config.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['password.secret']}")
    public String passwordSecret;

    @Value("#{environment['password.iteration']}")
    public Integer passwordIteration;

    @Value("#{environment['session.secret']}")
    public String sessionSecret;

    @Value("#{environment['session.iteration']}")
    public Integer sessionIteration;

    @Value("#{environment['application.version']}")
    public String applicationVersion;

    @Value("#{environment['developer.name']}")
    public String developerName;

    @Value("#{environment['developer.email']}")
    public String developerEmail;

    @Value("#{environment['server.host']}")
    public String serverHost;

    @Value("#{environment['server.port']}")
    public Integer serverPort;

    @Value("#{environment['jdbc.user']}")
    public String jdbcUser;

    @Value("#{environment['jdbc.password']}")
    public String jdbcPassword;

    @Value("#{environment['jdbc.url']}")
    public String jdbcUrl;

    @Value("#{environment['jdbc.driver']}")
    public String jdbcDriver;

    @Value("#{environment['jdbc.sql_dialect']}")
    public String jdbcSqlDialect;

    @Value("#{environment['jdbc.hbm2ddl_auto']}")
    public String jdbcNbm2ddlAuto;

    @Value("#{environment['jdbc.show_sql']}")
    public String jdbcShowSql;

    @Value("#{environment['hibernate.cache.use_second_level_cache']}")
    public String hibernateCacheUseSecondLevelCache;

    @Value("#{environment['hibernate.cache.provider_configuration_file_resource_path']}")
    public String hibernateCacheProviderConfigurationFileResourcePath;

    @Value("#{environment['hibernate.cache.region.factory_class']}")
    public String hibernateCacheRegionFactoryClass;

    @Value("#{environment['hibernate.cache.use_query_cache']}")
    public String hibernateCacheUseQueryCache;

    @Value("#{environment['hibernate.cache.use_minimal_puts']}")
    public String hibernateCacheUseMinimalPuts;

    @Value("#{environment['hibernate.cache.hazelcast.use_lite_member']}")
    public String hibernateCacheHazelcastUseLiteMember;

    @Value("#{environment['hibernate.cache.region_prefix']}")
    public String hibernateCacheRegionPrefix;

}
