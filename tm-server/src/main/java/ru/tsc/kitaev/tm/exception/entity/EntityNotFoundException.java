package ru.tsc.kitaev.tm.exception.entity;

import ru.tsc.kitaev.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error. Entity not found.");
    }

}
