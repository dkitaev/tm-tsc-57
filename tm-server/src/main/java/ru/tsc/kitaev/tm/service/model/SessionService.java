package ru.tsc.kitaev.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.repository.model.ISessionRepository;
import ru.tsc.kitaev.tm.api.service.ILoggerService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.model.ISessionService;
import ru.tsc.kitaev.tm.api.service.model.IUserService;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.exception.user.AccessDeniedException;
import ru.tsc.kitaev.tm.exception.user.UserIsLockedException;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.util.HashUtil;

import javax.persistence.EntityManager;

@Service
@AllArgsConstructor
public final class SessionService extends AbstractService implements ISessionService {

    @NotNull
    public ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ILoggerService logService;

    @NotNull
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        @Nullable final User user = checkDataAccess(login, password);
        if (user == null) throw new AccessDeniedException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        final Session session = new Session();
        @Nullable final User user2 = userService.findByLogin(login);
        session.setUser(user2);
        session.setTimestamp(System.currentTimeMillis());
        sign(session);
        try {
            entityManager.getTransaction().begin();
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
        return session;
    }

    @Nullable
    @Override
    public User checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final User user = userService.findByLogin(login);
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(secret, iteration, password);
        if (user.getLocked()) throw new UserIsLockedException();
        if (user.getPasswordHash().equals(hash)) {
            return user;
        } else {
            return null;
        }
    }

    @NotNull
    @Override
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final String secret = propertyService.getSessionSecret();
        @NotNull final Integer iteration = propertyService.getSessionIteration();
        @Nullable final String signature = HashUtil.sign(secret, iteration, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void close(@Nullable Session session) {
        if (session == null) return;
        @NotNull final ISessionRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessionRepository.removeById(session.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DatabaseOperationException();
        } finally {
            entityManager.close();
        }
    }

}
