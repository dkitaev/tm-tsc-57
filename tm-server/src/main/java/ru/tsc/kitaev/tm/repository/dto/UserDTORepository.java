package ru.tsc.kitaev.tm.repository.dto;

import lombok.AllArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class UserDTORepository extends AbstractDTORepository implements IUserDTORepository {

    @Override
    public void add(@NotNull UserDTO user) {
        entityManager.persist(user);
    }

    @Override
    public void update(@NotNull UserDTO user) {
        entityManager.merge(user);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM UserDTO")
                .executeUpdate();
    }

    @Override
    public void remove(@NotNull UserDTO user) {
        entityManager.remove(user);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return entityManager
                .createQuery("FROM UserDTO", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDTO findById(@NotNull String id) {
        return getResult(entityManager
                .createQuery("FROM UserDTO u WHERE u.id = :id", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id));
    }

    @NotNull
    @Override
    public UserDTO findByIndex(@NotNull Integer index) {
        return entityManager
                .createQuery("FROM UserDTO", UserDTO.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull String login) {
        return getResult(entityManager
                .createQuery("FROM UserDTO u WHERE u.login = :login", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login));
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull String email) {
        return getResult(entityManager
                .createQuery("FROM UserDTO u WHERE u.email = :email", UserDTO.class)
                .setParameter("email", email));
    }

    @Nullable
    @Override
    public UserDTO getResult(@NotNull TypedQuery<UserDTO> query) {
        final List<UserDTO> result = query.getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

}
