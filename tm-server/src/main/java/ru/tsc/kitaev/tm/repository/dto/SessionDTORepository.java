package ru.tsc.kitaev.tm.repository.dto;

import lombok.AllArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kitaev.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.kitaev.tm.dto.SessionDTO;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class SessionDTORepository extends AbstractDTORepository implements ISessionDTORepository {

    @Override
    public void add(@NotNull SessionDTO session) {
        entityManager.persist(session);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM SessionDTO")
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<SessionDTO> findAll() {
        return entityManager
                .createQuery("FROM SessionDTO", SessionDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findById(@NotNull String id) {
        return entityManager
                .createQuery("FROM SessionDTO s WHERE s.id = :id", SessionDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager.remove(findById(id));
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(s) FROM ProjectDTO s", Long.class)
                .getSingleResult()
                .intValue();
    }

}
