package ru.tsc.kitaev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;

public interface IRepository {

    @NotNull
    EntityManager getEntityManager();

}
