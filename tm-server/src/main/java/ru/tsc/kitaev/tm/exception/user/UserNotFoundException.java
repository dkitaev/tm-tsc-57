package ru.tsc.kitaev.tm.exception.user;

import ru.tsc.kitaev.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error. User not found.");
    }

}
